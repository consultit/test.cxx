/**
 * sla.h
 *
 *  Created on: Sep 8, 2019
 *      Author: consultit
 */

#ifndef TEMPLATE_MP_SRC_SLA_H_
#define TEMPLATE_MP_SRC_SLA_H_

#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>

// SLA
void adjust_values(double*, double*, double*, double*);

// New Application
struct Location
{
	double l = 1.0;
};

struct Time
{
	double t = 0.0;
};

class Reading
{
	/* stuff */
public:
	double alpha_value(Location l, Time t) const
	{
		return l.l + 2.0 * t.t;
	}
	double beta_value(Location l, Time t) const
	{
		return l.l + 3.0 * t.t;
	}
	/* other stuff */
};

// pragmatic approach
std::tuple<double, double, double, double> get_adjusted_values_pragmatic(
		const Reading&, Location, Time, Time);

// generic approach 1
template<typename Reading>
std::tuple<double, double, double, double> get_adjusted_values_generic_1(
		const Reading &r, Location l, Time t1, Time t2)
{
	double alpha1 = r.alpha_value(l, t1);
	double beta1 = r.beta_value(l, t1);
	double alpha2 = r.alpha_value(l, t2);
	double beta2 = r.beta_value(l, t2);
	adjust_values(&alpha1, &beta1, &alpha2, &beta2);
	return std::make_tuple(alpha1, beta1, alpha2, beta2);
}

// generic approach 2
template<typename AlphaValue, typename BetaValue>
std::tuple<double, double, double, double> get_adjusted_values_generic_2(
		AlphaValue alpha_value, BetaValue beta_value, Location l, Time t1,
		Time t2)
{
	double alpha1 = alpha_value(l, t1);
	double beta1 = beta_value(l, t1);
	double alpha2 = alpha_value(l, t2);
	double beta2 = beta_value(l, t2);
	adjust_values(&alpha1, &beta1, &alpha2, &beta2);
	return std::make_tuple(alpha1, beta1, alpha2, beta2);
}

// Extracting the C Function’s Parameters
template<typename F>
struct make_tuple_of_params;

template<typename Ret, typename ... Args>
struct make_tuple_of_params<Ret(Args...)>
{
	using type = std::tuple<typename std::remove_pointer<Args>::type ...>;
	using typeP = std::tuple<typename std::add_pointer<Args>::type ...>;
};
template<typename Ret, typename ... Args>
struct make_tuple_of_params<Ret (*)(Args...)>
{
	using type = std::tuple<typename std::remove_pointer<Args>::type ...>;
	using typeP = std::tuple<typename std::add_pointer<Args>::type ...>;
};
// convenience function
template<typename F> using make_tuple_of_params_t = typename make_tuple_of_params<F>::type;

template<std::size_t FunctionIndex,
		typename FunctionsTuple,
		typename Params,
		std::size_t ... I>
auto dispatch_params(const FunctionsTuple &functions, const Params &params,
		std::index_sequence<I...>)
{
	auto f = std::get<FunctionIndex>(functions);
	return f(std::get<I>(params)...);
}

template<typename FunctionsTuple,
		std::size_t ... I,
		typename Params,
		typename ParamsSeq>
auto dispatch_functions(const FunctionsTuple &functions,
						std::index_sequence<I...>,
						const Params &params,
						ParamsSeq params_seq)
{
	return std::make_tuple(dispatch_params<I>(functions,
			params,
			params_seq)...);
}

double legacyFunc(double a, double b, double c, double d);

template<typename F,
		typename Tuple,
		std::size_t ... I>
void dispatch_to_c(F f, Tuple &t, std::index_sequence<I...>)
{
	f(&std::get<I>(t)...);
}
// magic_wand
template<typename LegacyFunction,
		typename ... Functions,
		typename ... Params>
auto magic_wand(LegacyFunction legacy,
		const std::tuple<Functions...> &functions,
		const std::tuple<Params...> &params1,
		const std::tuple<Params...> &params2)
{
	static const std::size_t functions_count = sizeof...(Functions);
	static const std::size_t params_count = sizeof...(Params);

	using tuple_type = make_tuple_of_params_t<LegacyFunction>;

	tuple_type params =
		std::tuple_cat(
			dispatch_functions(functions,
					std::make_index_sequence<functions_count>(),
					params1,
					std::make_index_sequence<params_count>()),
			dispatch_functions(functions,
					std::make_index_sequence<functions_count>(),
					params2,
					std::make_index_sequence<params_count>()));

	static const std::size_t t_count = std::tuple_size<tuple_type>::value;
	dispatch_to_c(legacy, params, std::make_index_sequence<t_count>());
	return params;
}

template<typename Reading>
std::tuple<double, double, double, double> get_adjusted_values(Reading &r,
		Location l, Time t1, Time t2)
{
	return magic_wand(adjust_values,
			std::make_tuple([&r](Location l, Time t)
							{
								return r.alpha_value(l, t);
							},
							[&r](Location l, Time t)
							{
								return r.beta_value(l, t);
							}),
			std::make_tuple(l, t1),
			std::make_tuple(l, t2));
}

#endif /* TEMPLATE_MP_SRC_SLA_H_ */
