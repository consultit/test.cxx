/**
 * main.cpp
 *
 *  Created on: Aug 29, 2019
 *      Author: consultit
 */

#include "sla.h"

int main(int argc, char **argv)
{
	Reading r;
	Location l;
	Time t1, t2;
	t1.t = 1.0;
	t2.t = 2.0;
	// pragmatic approach
	std::apply([](auto &&... args)
	{	((std::cout << args << std::endl), ...);},
			get_adjusted_values_pragmatic(r, l, t1, t2));
	// generic approach 1
	std::apply([](auto &&... args)
	{	((std::cout << args << std::endl), ...);},
			get_adjusted_values_generic_1(r, l, t1, t2));
	// generic approach 2
	std::apply([](auto &&... args)
	{	((std::cout << args << std::endl), ...);},
			get_adjusted_values_generic_2([&r](Location l, Time t)
			{	return r.alpha_value(l, t);}, [&r](Location l, Time t)
			{	return r.beta_value(l, t);}, l, t1, t2));
	// double_ptr_type will be double *
	using double_ptr_type = std::add_pointer<double>::type;
	// double_type will be double
	using double_type = std::remove_pointer<double *>::type;
	// note that removing a pointer from a nonpointer type is safe
	// the type of double_too_type is double
	using double_too_type = std::remove_pointer<double>::type;
	double_type pi = 314.159e-2;
	double_too_type e = 0.271828e1;
	double_ptr_type pip = &pi, pe = &e;
	std::apply([](auto &&... args)
	{	((std::cout << args << std::endl), ...);},
			std::make_tuple(pi, e, *pip, *pe));
	// test make_tuple_of_params
	int i = 10;
	double d = 20.0e-10;
	make_tuple_of_params<void(int*, double*)>::type a = std::make_tuple(i, d);
	make_tuple_of_params_t<void(int*, double*)> b = a;
	std::cout << "(" << std::get<0>(b) << ", " << std::get<1>(b) << ")"
			<< std::endl;
	make_tuple_of_params<void(int, double)>::typeP pa = std::make_tuple(&i, &d);
	std::cout << "(" << *std::get<0>(pa) << ", " << *std::get<1>(pa) << ")"
			<< std::endl;
	// dispatch_params
	auto params = std::make_tuple(1.0, 2.0, 3.0, 4.0);
	auto idx = std::make_index_sequence<4>{};
	auto idx1 = std::index_sequence<0, 1, 2, 3>{};
	auto functions = std::make_tuple(legacyFunc);
	std::cout << dispatch_params<0>(functions, params, idx) << std::endl;
	std::cout << dispatch_params<0>(std::make_tuple(legacyFunc),
			std::make_tuple(1.0, 2.0, 3.0, 4.0), idx1) << std::endl;
	// Putting It All Together
	std::apply(
			[](auto &&... args)
			{
				((std::cout << args << std::endl), ...);
			},
			get_adjusted_values(r, l, t1, t2)
	);

	return 0;
}
