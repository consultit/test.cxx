/**
 * main_test01.cpp
 *
 *  Created on: Sep 8, 2019
 *      Author: consultit
 */

#include "test01.h"

int main(int argc, char **argv)
{
	{
		constexpr auto o = cpp_mp::one.value;
		std::cout << o << std::endl;
	}
	{
		// TypeContainer::size
		constexpr auto s0 = cpp_mp::size<
				std::tuple<char, short, int, long int, long long int>>::value;
		std::cout << s0 << std::endl;
		constexpr auto s1 =
				cpp_mp::size<
						cpp_mp::TypeContainer1<char, short, int, long int,
								long long int>>::value;
		std::cout << s1 << std::endl;
	}
	{
		// TypeContainer::push_back
		using tPushBack = cpp_mp::push_back<cpp_mp::TypeContainer1<char, short, int, long int>, long long int>::type;
		std::cout << cpp_mp::size<tPushBack>::value << std::endl;
		// TypeContainer::push_front
		using tPushFront = cpp_mp::push_back<cpp_mp::TypeContainer1<short, int, long int, long long int>, char>::type;
		std::cout << cpp_mp::size<tPushFront>::value << std::endl;
		// TypeContainer::pop_front
		using tPopFront = cpp_mp::pop_front<cpp_mp::TypeContainer1<long long int, char, short, int, long int, long long int>>::type;
		std::cout << cpp_mp::size<tPopFront>::value << std::endl;
	}
	{
		// TypeContainer::rename
		using rInContainer = cpp_mp::TypeContainer1<char, short, int, long int, long long int>;
		using rOutContainer = cpp_mp::TypeContainer2<>;
		std::cout
				<< cpp_mp::size<
						cpp_mp::rename<rInContainer, rOutContainer>::type>::value
				<< std::endl;
	}
	{
		// TypeContainer::append
		using aContainer = cpp_mp::TypeContainer1<char, short, int, long int, long long int>;
		using aOtherContainer = cpp_mp::TypeContainer2<>;
		std::cout
				<< cpp_mp::size<
						cpp_mp::append<aContainer, aOtherContainer>::type>::value
				<< std::endl;
	}
	{
		// TypeContainer::lambda
		using lMetaFunc = typename cpp_mp::lambda<cpp_mp::TypeContainer1>;
		std::cout
				<< cpp_mp::size<
						lMetaFunc::apply<char, short, int, long int,
								long long int>::type>::value << std::endl;
		using lMetaFunc1 = typename cpp_mp::lambda<std::remove_pointer>;
		std::cout << cpp_mp::size<lMetaFunc1::apply<char*>>::value << std::endl;
	}
	{
		// TypeContainer::transform
		using tContainerFrom = std::tuple<char, short, int, long int, long long int>;
		using tLambda = cpp_mp::lambda<std::add_pointer>;
		using tContainerTo = cpp_mp::transform<tContainerFrom, tLambda>;
		using tContainerPtr = typename tContainerTo::type;
		char c = '1';
		short s = 20;
		int i = 300;
		long int li = 4000L;
		long long int lli = 50000LL;
		tContainerPtr tuplePtr = std::make_tuple(&c, &s, &i, &li, &lli);
		std::apply([](auto &&... args)
		{	((std::cout << *args << std::endl), ...);}, tuplePtr);
	}

	return 0;
}
