/**
 * sla.cpp
 *
 *  Created on: Sep 8, 2019
 *      Author: consultit
 */

#include "sla.h"

// SLA
void adjust_values(double *alpha1, double *beta1, double *alpha2, double *beta2)
{
	*alpha1 *= 2.0;
	*beta1 *= 0.5;
	*alpha2 *= 4.0;
	*beta2 *= 0.25;
}

// New Application
// pragmatic approach
std::tuple<double, double, double, double> get_adjusted_values_pragmatic(
		const Reading &r, Location l, Time t1, Time t2)
{
	double alpha1 = r.alpha_value(l, t1);
	double beta1 = r.beta_value(l, t1);
	double alpha2 = r.alpha_value(l, t2);
	double beta2 = r.beta_value(l, t2);
	adjust_values(&alpha1, &beta1, &alpha2, &beta2);
	return std::make_tuple(alpha1, beta1, alpha2, beta2);
}

double legacyFunc(double a, double b, double c, double d)
{
	return a + b + c + d;
}
