/**
 * test01.h
 *
 *  Created on: Aug 29, 2019
 *      Author: consultit
 */

#ifndef TEMPLATE_MP_SRC_TEST01_H_
#define TEMPLATE_MP_SRC_TEST01_H_

#include <iostream>
#include <type_traits>
#include <string>
#include <vector>
#include <tuple>
#include <memory>

namespace cpp_mp
{

extern std::integral_constant<int, 1> one;

template<typename ...T> void f_type(T ...args)
{
}
template<int ...idx> void f_non_type()
{
	std::vector<int> iArray
	{ idx... };
}

template<typename ...T> struct meta_type_list
{
	void mf(T ...)
	{
	}
};
using list_of_int_types = meta_type_list<char,short,int, long int, long long int>;

template<int ...idx> struct meta_non_type_list
{
	std::vector<int> iArray
	{ idx... };
};
using list_of_ints = meta_non_type_list<0, 1, 2, 3>;

template<typename A, typename B> struct alfa
{
};
template<typename C, typename D> struct beta
{
};
template<template<typename A, typename B> typename...Tmpl> struct meta_template_list
{
	void mf(const Tmpl<char, int> &...args)
	{
	}
};
using list_of_templates = meta_template_list<alfa, beta>;

// TypeContainer::size
template<typename Container> struct size;

template<
	template<typename ...> typename Container,
	typename ... Types>
struct size<Container<Types...>>: std::integral_constant<std::size_t, sizeof...(Types)>
{};

// TypeContainer::push_back
template<typename Container, typename Type> struct push_back;

template<
	template<typename ...> typename Container,
	typename ... Types, typename Type>
struct push_back<Container<Types...>, Type>
{
	using type = Container<Types..., Type>;
};

// TypeContainer::push_front
template<typename Container, typename Type> struct push_front;

template<
	template<typename ...> typename Container,
	typename ... Types, typename Type>
struct push_front<Container<Types...>, Type>
{
	using type = Container<Type, Types...>;
};

// TypeContainer::pop_front
template<typename Container> struct pop_front;

template<
	template<typename ...> typename Container,
	typename Head, typename ... Types>
struct pop_front<Container<Head, Types...>>
{
	using type = Container<Types...>;
};

template<template<typename ...> typename Container>
struct pop_front<Container<>>
{
	using type = Container<>;
};

// TypeContainer::rename
template<typename InContainer, typename OutContainer> struct rename;

template<
	template<typename ...> typename InContainer,
	template<typename ...> typename OutContainer,
	typename ... InTypes, typename ... OutTypes>
struct rename<InContainer<InTypes...>, OutContainer<OutTypes...> >
{
	using type = OutContainer<InTypes...>;
};

// TypeContainer::append
template<typename Container, typename OtherContainer> struct append;

template<
	template<typename ...> typename Container,
	template<typename ...> typename OtherContainer,
	typename ... Types, typename ... OtherTypes>
struct append<Container<Types...>, OtherContainer<OtherTypes...> >
{
	using type = Container<Types..., OtherTypes...>;
};

// TypeContainer::lambda
template<template<typename ...> typename MetaFunction>
struct lambda
{
	template<typename ... Types> struct apply
	{
		using type = typename MetaFunction<Types ...>::type;
	};
};

// TypeContainer::transform
template<typename Container, typename Lambda> struct transform;

template<
	template<typename ...> typename Container, typename ... Types,
	typename Lambda>
struct transform<Container<Types ...>, Lambda>
{
	using type = Container< typename Lambda::template apply<Types>::type ... >;
};

// Custom TypeContainers
template<typename ... Types> struct TypeContainer1
{
	using type = TypeContainer1<Types ...>;
};
template<typename ... Types> struct TypeContainer2
{
	using type = TypeContainer2<Types ...>;;
};

}
#endif /* TEMPLATE_MP_SRC_TEST01_H_ */
