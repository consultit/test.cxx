// Generated by Bisonc++ V4.09.02 on Sat, 13 Feb 2016 11:12:54 +0100

#ifndef elyParser_h_included
#define elyParser_h_included

// $insert baseclass
#include "Parserbase.h"
// $insert scanner.h
#include "Scanner.h"

#include <map>
#include <string>

// $insert namespace-open
namespace ely
{

#undef Parser
class Parser: public ParserBase
{
	friend Scanner;

	// $insert scannerobject
	Scanner d_scanner;

public:
	Parser()
	{
		d_scanner.setParser(this);
	}
	Parser(std::istream &in)
	{
		d_scanner.switchStreams(in);
		d_scanner.setParser(this);
	}

	int parse();

private:
	void error(char const *msg);    // called on (syntax) errors
	int lex();                      // returns the next token from the
									// lexical scanner.
	void print();                   // use, e.g., d_token, d_loc

	// support functions for parse():
	void executeAction(int ruleNr);
	void errorRecovery();
	int lookup(bool recovery);
	void nextToken();
	void print__();
	void exceptionHandler__(std::exception const &exc);

	// symbol tables
	std::map<std::string, double> d_symbols;
	static std::map<std::string, double (*)(double)> s_functions;
	typedef std::pair<char const *, double (*)(double)> FunctionPair;
	static FunctionPair s_funTab[];
};

// $insert namespace-close
}

#endif
