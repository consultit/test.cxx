/*
 *   This file is part of test.
 *
 *   test is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   test is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with test.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file /test/src/main.cpp
 *
 * \date 28/apr/2014 (10:48:52)
 * \author consultit
 */

#include "Parser.h"
#include <fstream>

using namespace std;

int main(int argc, char **argv)
{
	if (argc < 2)
	{
		cout << "Usage: " << argv[0] << " i | <FILE> " << endl;
		exit(-1);
	}
	string arg1(argv[1]);
	if (arg1 == "i")
	{
		//interactive
		ely::Parser parser;
		parser.parse();
	}
	else
	{
		//read file
		ifstream inFile(arg1);
		if (not inFile.is_open())
		{
			cout << "Cannot open: " << argv[1] << endl;
			exit(-2);
		}
		ely::Parser parser(inFile);
		parser.parse();
	}

	return 0;
}
