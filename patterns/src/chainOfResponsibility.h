/**
 * chainOfResponsibility.h
 *
 *  Created on: Jun 14, 2019
 *      Author: consultit
 */

#ifndef PATTERNS_SRC_CHAINOFRESPONSIBILITY_H_
#define PATTERNS_SRC_CHAINOFRESPONSIBILITY_H_

#include <iostream>
#include <vector>
#include <ctime>

using namespace std;

namespace Before
{
// Purpose. Chain of Responsibility.
//
// Discussion. Instead of the client
// having to know about the number of
// "handlers", and manually map re-
// quests to available handlers; de-
// sign the handlers into an "intel-
// ligent" chain. Clients "launch
// and leave" requests with the head
// of the chain.
class H
{
public:
	H()
	{
		id_ = count_++;
		busy_ = false;
	}
	~H()
	{
		cout << id_ << " dtor" << endl;
	}
	int handle()
	{
		if ((busy_ = !busy_))
		{
			cout << id_ << " handles" << endl;
			return 1;
		}
		else
		{
			cout << id_ << " is busy" << endl;
			return 0;
		}
	}
private:
	int id_;
	bool busy_;
	static int count_;
};

} /* namespace Before */

namespace ChainOfResp
{

class H
{
public:
	H(H* next = 0)
	{
		id_ = count_++;
		busy_ = false;
		next_ = next;
	}
	~H()
	{
		cout << id_ << " dtor" << endl;
		// don't get into a loop
		if (next_->next_)
		{
			H* t = next_;
			next_ = 0;
			delete t;
		}
	}
	void setNext(H* next)
	{
		next_ = next;
	}
	void handle()
	{
		if ((busy_ = !busy_))
			cout << id_ << " handles" << endl;
		else
		{
			cout << id_ << " is busy" << endl;
			next_->handle();
		}
	}
private:
	int id_;
	bool busy_;
	H* next_;
	static int count_;
};

inline H* setUpList()
{
	H* first = new H;
	H* last = new H(first);
	first->setNext(last);
	return first;
}

// Purpose. Chain of Responsibility design pattern
// 1. Put a "next" pointer in the base class
// 2. The "chain" method in the base class always delegates to the next object
// 3. If the derived classes cannot handle, they delegate to the base class
class Base
{
	// 1. "next" pointer in the base class
	Base* next;
public:
	Base()
	{
		next = 0;
	}
	virtual ~Base()
	{
	}
	void setNext(Base* n)
	{
		next = n;
	}
	void add(Base* n)
	{
		if (next)
			next->add(n);
		else
			next = n;
	}
	// 2. The "chain" method in the base class always delegates to the next obj
	virtual void handle(int i)
	{
		next->handle(i);
	}
};

class Handler1: public Base
{
public:
	/*virtual*/
	void handle(int i)
	{
		if (rand() % 3)
		{ // 3. Don't handle requests 3 times out of 4
			cout << "H1 passed " << i << " ";
			Base::handle(i); // 3. Delegate to the base class
		}
		else
			cout << "H1 handled " << i << " ";
	}
};

class Handler2: public Base
{
public:
	/*virtual*/
	void handle(int i)
	{
		if (rand() % 3)
		{
			cout << "H2 passed " << i << " ";
			Base::handle(i);
		}
		else
			cout << "H2 handled " << i << " ";
	}
};

class Handler3: public Base
{
public:
	/*virtual*/
	void handle(int i)
	{
		if (rand() % 3)
		{
			cout << "H3 passed " << i << " ";
			Base::handle(i);
		}
		else
			cout << "H3 handled " << i << " ";
	}
};

// Purpose. Chain of Responsibility and Composite
// 1. Put a "next" pointer in the base class
// 2. The "chain" method in the base class always delegates to the next object
// 3. If the derived classes cannot handle, they delegate to the base class
class Component
{
	int value;
	// 1. "next" pointer in the base class
	Component* next;
public:
	Component(int v, Component* n)
	{
		value = v;
		next = n;
	}
	virtual ~Component()
	{
	}
	void setNext(Component* n)
	{
		next = n;
	}
	virtual void traverse()
	{
		cout << value << ' ';
	}
	// 2. The "chain" method in the base class always delegates to the next obj
	virtual void volunteer()
	{
		next->volunteer();
	}
};

class Primitive: public Component
{
public:
	Primitive(int val, Component* n = 0) :
			Component(val, n)
	{
	}
	/*virtual*/
	void volunteer()
	{
		Component::traverse();
		// 3. Primitive objects don't handle volunteer requests 5 times out of 6
		if (rand() * 100 % 6 != 0)
			// 3. Delegate to the base class
			Component::volunteer();
	}
};

class Composite: public Component
{
	vector<Component*> children;
public:
	Composite(int val, Component* n = 0) :
			Component(val, n)
	{
	}
	void add(Component* c)
	{
		children.push_back(c);
	}
	/*virtual*/
	void traverse()
	{
		Component::traverse();
		for (int i = 0; i < (int) children.size(); i++)
			children[i]->traverse();
	}
	// 3. Composite objects never handle volunteer requests
	/*virtual*/
	void volunteer()
	{
		Component::volunteer();
	}
};

///
// Purpose. Chain of Responsibility - links bid on job, chain assigns job
//
// 1. Base class maintains a "next" pointer
// 2. Each "link" does its part to handle (and/or pass on) the request
// 3. Client "launches and leaves" each request with the chain
// 4. The current bid and bidder are maintained in chain static members
// 5. The last link in the chain assigns the job to the low bidder
class Link
{
	int id;
	// 1. "next" pointer
	Link* next;
	// 4. Current bid and bidder
	static int theBid;
	static Link* bidder;
public:
	Link(int num)
	{
		id = num;
		next = 0;
	}
	void addLast(Link* l)
	{
		if (next)
			// 2. Handle and/or pass on
			next->addLast(l);
		else
			next = l;
	}
	void bid()
	{
		int num = rand() % 9;
		cout << id << '-' << num << " ";
		if (num < theBid)
		{
			// 4. Current bid and bidder
			theBid = num;
			bidder = this;
		}
		if (next)
			// 2. Handle and/or pass on
			next->bid();
		else
			// 5. The last 1 assigns the job
			bidder->execute();
	}
	void execute()
	{
		cout << id << " is executing\n";
		theBid = 999;
	}
};

} /* namespace ChainOfResp */

#endif /* PATTERNS_SRC_CHAINOFRESPONSIBILITY_H_ */
