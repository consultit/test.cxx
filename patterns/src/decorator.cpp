/**
 * decorator.cpp
 *
 *  Created on: Apr 28, 2019
 *      Author: consultit
 */

#include "decorator.h"

int main(int argc, char **argv)
{
	// inheritance
	{
		inheritance::AwithX anX;
		inheritance::AwithXY anXY;
		inheritance::AwithXYZ anXYZ;
		anX.do_it();
		cout << '\n';
		anXY.do_it();
		cout << '\n';
		anXYZ.do_it();
		cout << '\n';
	}
	// decorator
	{
		decorator::I* anX = new decorator::X(new decorator::A);
		decorator::I* anXY = new decorator::Y(
				new decorator::X(new decorator::A));
		decorator::I* anXYZ = new decorator::Z(
				new decorator::Y(new decorator::X(new decorator::A)));
		anX->do_it();
		cout << '\n';
		anXY->do_it();
		cout << '\n';
		anXYZ->do_it();
		cout << '\n';
		delete anX;
		delete anXY;
		delete anXYZ;
	}
	return 0;
}

