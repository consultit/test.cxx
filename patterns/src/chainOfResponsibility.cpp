/**
 * chainOfResponsibility.cpp
 *
 *  Created on: Jun 14, 2019
 *      Author: consultit
 */

#include "chainOfResponsibility.h"

namespace Before
{
int H::count_ = 1;
} /* namespace After */

namespace ChainOfResp
{
int H::count_ = 1;
///
int Link::theBid = 999; // 4. Current bid and bidder
Link* Link::bidder = 0;
} /* namespace After */

int main(int argc, char **argv)
{
	// Before
	{
		using namespace Before;
		const int TOTAL = 2;
		H* list[TOTAL] =
		{ new H, new H };
		for (int i = 0; i < 6; i++)
			for (int j = 0; 1; j = (j + 1) % TOTAL)
				if (list[j]->handle())
					break;
		for (int k = 0; k < TOTAL; k++)
			delete list[k];
	}
	// ChainOfResp
	{
		using namespace ChainOfResp;
		/// Chain of Responsibility design pattern
		{
			H* head = setUpList();
			for (int i = 0; i < 6; i++)
				head->handle();
			delete head;

			///
			srand(time(0));
			Handler1 root;
			Handler2 two;
			Handler3 thr;
			root.add(&two);
			root.add(&thr);
			thr.setNext(&root);
			for (int i = 1; i < 10; i++)
			{
				root.handle(i);
				cout << '\n';
			}
		}

		/// Chain of Responsibility and Composite
		{
			srand(time(0));						// 1
			Primitive seven(7);					// |
			Primitive six(6, &seven);			// |
			Composite three(3, &six);			// +-- 2
			three.add(&six);					// |   |
			three.add(&seven);					// |   +-- 4 5
			Primitive five(5, &three);			// |
			Primitive four(4, &five);			// |
			Composite two(2, &four);			// +-- 3
			two.add(&four);						// |   |
			two.add(&five);						// |   +-- 6 7
			Composite one(1, &two);				// |
			Primitive nine(9, &one);			// +-- 8 9
			Primitive eight(8, &nine);
			one.add(&two);
			one.add(&three);
			one.add(&eight);
			one.add(&nine);
			seven.setNext(&eight);
			cout << "traverse: ";
			one.traverse();
			cout << '\n';
			for (int i = 0; i < 8; i++)
			{
				one.volunteer();
				cout << '\n';
			}
		}

		/// Chain of Responsibility - links bid on job, chain assigns job
		{
			Link chain(1);
			for (int i = 2; i < 7; i++)
				chain.addLast(new Link(i));
			srand(time(0));
			for (int i = 0; i < 10; i++)
				chain.bid(); // 3. Client "launches & leaves"
		}
	}
	return 0;
}
