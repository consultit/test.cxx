/*
 *   This file is part of test.
 *
 *   test is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   test is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with test.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file /test/src/main.cpp
 *
 * \date 28/apr/2014 (10:48:52)
 * \author consultit
 */

#include <iostream>
#include <cmath>
#include <set>
#include <map>
#include <vector>
#include <random>
#include <chrono>

using namespace std;

struct Vector
{
	Vector()
	{
	}
	Vector(float _x, float _y, float _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}
	union
	{
		float coords[3];
		struct
		{
			float x, y, z;
		};
	};
	float& operator[](size_t i)
	{
		return coords[i % 3];
	}
	Vector operator+(const Vector& other)
	{
		Vector p;
		p.x = (*this).x + other.x;
		p.y = (*this).y + other.y;
		p.z = (*this).z + other.z;
		return p;
	}
	Vector operator-(const Vector& other)
	{
		Vector p;
		p.x = (*this).x - other.x;
		p.y = (*this).y - other.y;
		p.z = (*this).z - other.z;
		return p;
	}
};

struct Node;
struct Object
{
	// Center point for object
	Vector center;
	// Radius of object bounding sphere
	float radius;
	// Speed
	Vector speed;

	// Update position
	void update(float dt, Vector worldCenter, float maxWidth)
	{
		for (auto j = 0; j < 3; ++j)
		{
			center[j] += speed[j] * dt;
			float delta = center[j] - worldCenter[j];
			if (delta > maxWidth)
			{
				center[j] = worldCenter[j] + 2 * maxWidth - delta;
				speed[j] = -speed[j];
			}
			else if (delta < -maxWidth)
			{
				center[j] = worldCenter[j] - 2 * maxWidth - delta;
				speed[j] = -speed[j];
			}
		}
	}
	// Current container Node
	Node* pNode;
};

// Octree node data structure
struct Node
{
	// Center point of octree node
	Vector center;
	// Half the width of the node volume
	float halfWidth;
	// Parent
	Node *pParent;
	// Index relative to its Parent (0-7)
	int index;

	// Child node table indexed by index (0-7)
	map<int, Node*> pChildren;
	// Object set
	set<Object*> pObjects;
};

//
double (*Abs)(double) = &abs;
// Dynamic
bool InsertObject(Node *pTree, Object *pObject);
void DeleteOctree(Node* octree);
//
void TestAllCollisions(Node *pTree);
void TestCollision(Object *pA, Object *pB);

//some values
const Vector WORLDCENTER(0.0, 0.0, 0.0);
const float WORLDWIDTH = 200.0;
const int OBJECTSNUM = 2;
vector<Object> objects;
vector<Object>::iterator objectIt;
const float OBJECTMAXRADIUS = 5.0;
const float OBJECTMAXSPEED = 3.0;

int main(int argc, char** argv)
{
	const float WORLDHALFWIDTH = WORLDWIDTH / 2.0;
	// Create the octree
	Node* octree = new Node;
	octree->pParent = NULL;
	octree->index = -1;
	octree->center = WORLDCENTER;
	octree->halfWidth = WORLDHALFWIDTH;

	// Initialize randomness
	std::random_device rd("/dev/urandom");
	size_t rangeRND = rd.max() - rd.min();
	// Add Objects and initialize them
	for (auto i = 0; i < OBJECTSNUM; ++i)
	{
		// Add objects
		objects.push_back(Object());
		// Object starts by being (possibly) contained into octree root
		objects[i].pNode = octree;
		// set random position (between +/-(WORLDWIDTH/2.0 - radius))
		// set random speed (between +/- OBJECTMAXSPEED)
		for (auto j = 0; j < 3; ++j)
		{
			float rnd = (1.0 - 2.0 * (float) rd() / (float) rangeRND);
			objects[i].center[j] = rnd
					* (octree->halfWidth - OBJECTMAXRADIUS * 1.1);
			rnd = (1.0 - 2.0 * (float) rd() / (float) rangeRND);
			objects[i].speed[j] = rnd * OBJECTMAXSPEED;
		}
	}
	// Initialize Objects
	for (objectIt = objects.begin(); objectIt != objects.end(); ++objectIt)
	{
		// Object starts by being (possibly) contained into octree root
		objectIt->pNode = octree;
		// Other initializations
	}

	// Start the game loop: test all collisions
	bool goOn = true;
	chrono::steady_clock::time_point tPrev = chrono::steady_clock::now();
	while (goOn)
	{
		chrono::steady_clock::time_point tNow = chrono::steady_clock::now();
		float dt = chrono::duration_cast<chrono::duration<float>>(tNow - tPrev).count();
		tPrev = tNow;
		// Update Objects' positions
		for (objectIt = objects.begin(); objectIt != objects.end(); ++objectIt)
		{
			objectIt->update(dt, WORLDCENTER, WORLDHALFWIDTH);
		}

		///Tests for collisions
		// Update octree content: cycle over Objects
		for (objectIt = objects.begin(); objectIt != objects.end(); ++objectIt)
		{
			// Try first to insert Object into its current container node
			Node *containerNode = objectIt->pNode;
			while (not InsertObject(containerNode, &(*objectIt)))
			{
				// Try current container node's parent, if any
				if (containerNode->pParent)
				{
					containerNode = containerNode->pParent;
				}
				else
				{
					// Object cannot be inserted into this octree
					break;
				}
			}
		}
		// Test collisions
		TestAllCollisions(octree);
	}

	// Remove the octree
	DeleteOctree(octree);

	return 0;
}

//
void DeleteOctree(Node* octree)
{
	for (map<int, Node*>::const_iterator childrenIt = octree->pChildren.begin();
			childrenIt != octree->pChildren.end(); ++childrenIt)
	{
		DeleteOctree(childrenIt->second);
	}

	delete octree;
	octree = NULL;
}

bool InsertObject(Node *pTree, Object *pObject)
{
	// Check if Object is (partially) outside tree
	float delta = pTree->halfWidth - pObject->radius;
	if (delta <= 0.0)
	{
		// radius >= halfWidth: Object cannot be contained in tree
		return false;
	}
	else
	{
		// delta > 0.0: radius < halfWidth
		for (int i = 0; i < 3; i++)
		{
			if (Abs(pObject->center[i] - pTree->center[i]) >= delta)
			{
				// Object (partially) outside tree
				return false;
			}
		}
	}
	// Object insertion
	int index = 0;
	bool straddle = false;
	// Compute the octant number [0..7] the object sphere center is in
	// If straddling any of the dividing x, y, or z planes, exit directly
	for (int i = 0; i < 3; i++)
	{
		float delta = pObject->center[i] - pTree->center[i];
		if (Abs(delta) <= pObject->radius)
		{
			straddle = true;
			break;
		}
		else if (delta > 0.0f)
		{
			index |= (1 << i); // ZYX
		}
	}
	if (!straddle)
	{
		if (pTree->pChildren.find(index) == pTree->pChildren.end())
		{
			// Children[index] Node doesn't exist: create one
			Vector offset;
			float step = pTree->halfWidth * 0.5f;
			offset.x = ((index & 1) ? step : -step);
			offset.y = ((index & 2) ? step : -step);
			offset.z = ((index & 4) ? step : -step);
			Node* node = new Node;
			node->center = pTree->center + offset;
			node->halfWidth = step;
			node->index = index;
			node->pParent = pTree;
			pTree->pChildren[index] = node;
		}
		// Fully contained in existing Children[index] Node; insert in that subtree
		return InsertObject(pTree->pChildren[index], pObject);
	}
	else
	{
		// Straddling so add object into Children of this Node (if not already present)
		pTree->pObjects.insert(pObject);
		// Update the Object's current container Node, if needed
		if (pObject->pNode != pTree)
		{
			// pTree is the new container
			// Remove Object from current container Node
			pObject->pNode->pObjects.erase(pObject);

			// Delete current container Node, when
			// it has parent and has no objects and has no children
			if ((pObject->pNode->pParent)
					and (pObject->pNode->pObjects.size() == 0)
					and (pObject->pNode->pChildren.size() == 0))
			{
				// Remove Node from its parent's children
				pObject->pNode->pParent->pChildren.erase(pObject->pNode->index);
				// delete Node actually
				delete pObject->pNode;
			}

			// Update current container Node
			pObject->pNode = pTree;
		}
	}
	return true;
}

// Tests all objects that could possibly overlap due to cell ancestry and coexistence
// in the same cell. Assumes objects exist in a single cell only, and fully inside it
void TestAllCollisions(Node *pTree)
{
	// Keep track of all ancestor object lists in a stack
	const int MAX_DEPTH = 40;
	static Node *ancestorStack[MAX_DEPTH];
	static int depth = 0; // ’Depth == 0’ is invariant over calls
	// Check collision between all objects on this level and all
	// ancestor objects. The current level is included as its own
	// ancestor so all necessary pairwise tests are done
	ancestorStack[depth++] = pTree;
	for (int n = 0; n < depth; n++)
	{
		Object *pA, *pB;
		set<Object*>::const_iterator ancestorIt, treeIt;
		for (ancestorIt = ancestorStack[n]->pObjects.begin();
				ancestorIt != ancestorStack[n]->pObjects.end(); ++ancestorIt)
		{
			pA = *ancestorIt;
			for (treeIt = ancestorStack[n]->pObjects.begin();
					treeIt != ancestorStack[n]->pObjects.end(); ++treeIt)
			{
				pB = *treeIt;
				// Avoid testing both A->B and B->A
				if (pA == pB)
					break;
				// Now perform the collision test between pA and pB in some manner
				TestCollision(pA, pB);
			}
		}
	}
	// Recursively visit all existing children
	for (map<int, Node*>::const_iterator childrenIt = pTree->pChildren.begin();
			childrenIt != pTree->pChildren.end(); ++childrenIt)
	{
		TestAllCollisions(childrenIt->second);
	}

	// Remove current node from ancestor stack before returning
	depth--;
}

void TestCollision(Object *pA, Object *pB)
{
}

