# This file is part of the book "Modern Compiler Design, 2nd Ed." by
# Dick Grune, Henri Bal, Ceriel Jacobs and Koen Langendoen,
# of the Vrije Universiteit, Amsterdam.
# $Id: sysidf.mk,v 1.3 2012-05-21 08:33:13 Gebruiker Exp $

CC =		gcc
LEX =		flex
LINT =		lint -ansi -xh
TIME =		time

# UNIX version
# EXE =		#

# DOS version
EXE =		#.exe# not required by MinGW, but it produces .exe nevertheless
