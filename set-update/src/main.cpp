/*
 *   This file is part of test.
 *
 *   test is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   test is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with test.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file /test/src/main.cpp
 *
 * \date 28/apr/2014 (10:48:52)
 * \author consultit
 */

#include <iostream>
#include <sstream>
#include <set>
#include <list>
#include <algorithm>

using namespace std;

struct ON
{
	struct OND
	{
		long int pnode;
		string name;
		int count;
	};
	//main constructors
	ON(long int _pnode) :
			ond(new OND), refcount(new int)
	{
		ond->pnode = _pnode;
		*refcount = 1;
	}
	ON(long int _pnode, const string& _name, int _count) :
			ond(new OND), refcount(new int)
	{
		ond->pnode = _pnode;
		ond->name = _name;
		ond->count = _count;
		*refcount = 1;
	}
	//copy constructor
	ON(const ON& on) :
			ond(on.ond), refcount(on.refcount)
	{
		++(*refcount);
	}
	//destructor
	~ON()
	{
		if (--(*refcount) == 0)
		{
			delete ond;
			delete refcount;
		}
	}
	//assignment operator
	ON& operator=(const ON& on)
	{
		ond = on.ond;
		++(*refcount);
		return *this;
	}
	//comparison operator
	bool operator<(const ON& on) const
	{
		return ond->pnode < on.ond->pnode;
	}
	//owned resource
	OND* ond;
private:
	int* refcount;
};

set<ON> ps;
list<long int> cl;

int main(int argc, char** argv)
{
	int gcount = 0;

	ps.insert(ON(6, "6", 0));
	ps.insert(ON(4, "4", 0));
	ps.insert(ON(1, "1", 0));
	ps.insert(ON(3, "3", 0));
	ps.insert(ON(2, "2", 0));
	ps.insert(ON(5, "5", 0));

	cl.push_back(6);
	cl.push_back(8);
	cl.push_back(9);
	cl.push_back(7);
	cl.push_back(1);
	cl.push_back(2);

	set<ON>::const_iterator si;
	list<long int>::const_iterator li;

	cout << "begin" << endl;
	for (si = ps.begin(); si != ps.end(); ++si)
	{
		cout << (*si).ond->name << endl;
	}
	//elaborate current list: new and old elements
	//increment general count
	++gcount;
	cout << "begin insertion" << endl;
	for (li = cl.begin(); li != cl.end(); ++li)
	{
		//insert a default: check of equality is done only on pnode ond member (?)
		pair<set<ON>::iterator, bool> res = ps.insert(ON(*li));
		if (res.second)
		{
			(res.first)->ond->name =
					dynamic_cast<std::ostringstream&>(std::ostringstream().operator <<(
							*li)).str();
			(res.first)->ond->count = gcount;
			cout << (res.first)->ond->name << " is new " << endl;
		}
		else
		{
			cout << (res.first)->ond->name << " is old " << endl;
			///
			cout << "\t" << (res.first)->ond->count << " count pre" << endl;
			(res.first)->ond->count = gcount;
			cout << "\t" << (res.first)->ond->count << " count post" << endl;
			///
		}
	}
	cout << "end insertion" << endl;
	for (si = ps.begin(); si != ps.end(); ++si)
	{
		cout << (*si).ond->name << endl;
	}

	//erase out elements
	cout << "begin erase" << endl;
	for (si = ps.begin(); si != ps.end();)
	{
		if ((*si).ond->count != gcount)
		{
			cout << "erasing " << (*si).ond->name << endl;
			ps.erase(si++);
		}
		else
		{
			++si;
		}
	}
	cout << "end erase" << endl;
	for (si = ps.begin(); si != ps.end(); ++si)
	{
		cout << (*si).ond->name << endl;
	}

	return 0;
}

