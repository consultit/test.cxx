module employee;
import <iostream>;
import <format.h>; 
using namespace std;
using namespace fmt;

namespace Records {

	Employee::Employee(const string& firstName, const string& lastName) : m_firstName { firstName }, m_lastName { lastName }
	{ }

	void Employee::promote(int raiseAmount) {
		setSalary(getSalary() + raiseAmount);
	}
	void Employee::demote(int demeritAmount) {
		setSalary(getSalary() - demeritAmount);
	}

	void Employee::hire() {
		m_hired = true;
	}
	void Employee::fire() {
		m_hired = false;
	}

	void Employee::display() const {
		cout << format("Employee: {}, {}", getLastName(), getFirstName()) << endl;
		cout << "-------------------------" << endl;
		cout << (isHired() ? "Current Employee" : "Former Employee") << endl;
		cout << format("Employee Number: {}", getEmployeeNumber()) << endl;
		cout << format("Salary: ${}", getSalary()) << endl;
		cout << endl;
	}

	// Getters and setters
	void Employee::setFirstName(const string& value) {
		m_firstName = value;
	}
	const string& Employee::getFirstName() const {
		return m_firstName;
	}

	void Employee::setLastName(const string& value) {
		m_lastName = value;
	}
	const string& Employee::getLastName() const {
		return m_lastName;
	}
	
	void Employee::setEmployeeNumber(int value) {
		m_employeeNumber = value;
	}
	int Employee::getEmployeeNumber() const {
		return m_employeeNumber;
	}
	
	void Employee::setSalary(int value) {
		m_salary = value;
	}
	int Employee::getSalary() const {
		return m_salary;
	}
};


