/**
 * \file spam.cpp
 *
 * \date 2016-08-27
 * \author consultit
 */

#include <Python.h>
#include <structmember.h>
#include <string>

using namespace std;

typedef struct
{
	PyObject_HEAD
	PyObject *first; /* first name */
	PyObject *last; /* last name */
	int number;
	PyObject *callback;
} Spam;

namespace // anonymous
{

void Spam_dealloc(Spam* self)
{
	Py_XDECREF(self->first);
	Py_XDECREF(self->last);
	Py_XDECREF(self->callback);
	self->ob_type->tp_free((PyObject*) self);
}

PyObject *Spam_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
	Spam *self;

	self = reinterpret_cast<Spam *>(type->tp_alloc(type, 0));
	if (self != NULL)
	{
		self->first = PyString_FromString("");
		if (self->first == NULL)
		{
			Py_DECREF(self);
			return NULL;
		}

		self->last = PyString_FromString("");
		if (self->last == NULL)
		{
			Py_DECREF(self);
			return NULL;
		}

		self->number = 0;

		Py_INCREF(Py_None);
		self->callback = Py_None;
	}

	return (PyObject *) self;
}

int Spam_init(Spam *self, PyObject *args, PyObject *kwds)
{
	PyObject *first = NULL, *last = NULL, *callback = NULL, *tmp;

	static char *kwlist[] =
	{ const_cast<char*>("first"), const_cast<char*>("last"),
			const_cast<char*>("number"), const_cast<char*>("callback"),
			NULL };

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "|SSiO", kwlist, &first, &last,
			&self->number, &callback))
		return -1;

	if (first)
	{
		tmp = self->first;
		Py_INCREF(first);
		self->first = first;
		Py_DECREF(tmp);
	}

	if (last)
	{
		tmp = self->last;
		Py_INCREF(last);
		self->last = last;
		Py_DECREF(tmp);
	}

	if (callback && (PyCallable_Check(callback) || (callback == Py_None)))
	{
		tmp = self->callback;
		Py_INCREF(callback);
		self->callback = callback;
		Py_DECREF(tmp);
	}

	return 0;
}

PyMemberDef Spam_members[] =
{
{ const_cast<char*>("number"), T_INT, offsetof(Spam, number), 0,
		const_cast<char*>("noddy number") },
{ NULL } /* Sentinel */
};

PyObject * Spam_getfirst(Spam *self, void *closure)
{
	Py_INCREF(self->first);
	return self->first;
}

int Spam_setfirst(Spam *self, PyObject *value, void *closure)
{
	if (value == NULL)
	{
		PyErr_SetString(PyExc_TypeError, "Cannot delete the first attribute");
		return -1;
	}

	if (!PyString_Check(value))
	{
		PyErr_SetString(PyExc_TypeError,
				"The first attribute value must be a string");
		return -1;
	}

	Py_DECREF(self->first);
	Py_INCREF(value);
	self->first = value;

	return 0;
}

PyObject * Spam_getlast(Spam *self, void *closure)
{
	Py_INCREF(self->last);
	return self->last;
}

int Spam_setlast(Spam *self, PyObject *value, void *closure)
{
	if (value == NULL)
	{
		PyErr_SetString(PyExc_TypeError, "Cannot delete the last attribute");
		return -1;
	}

	if (!PyString_Check(value))
	{
		PyErr_SetString(PyExc_TypeError,
				"The last attribute value must be a string");
		return -1;
	}

	Py_DECREF(self->last);
	Py_INCREF(value);
	self->last = value;

	return 0;
}

PyObject * Spam_getcallback(Spam *self, void *closure)
{
	Py_INCREF(self->callback);
	return self->callback;
}

int Spam_setcallback(Spam *self, PyObject *value, void *closure)
{
	if (value == NULL)
	{
		PyErr_SetString(PyExc_TypeError,
				"Cannot delete the callback attribute");
		return -1;
	}

	if ((!PyCallable_Check(value)) && (value != Py_None))
	{
		PyErr_SetString(PyExc_TypeError,
				"The callback attribute value must be callable or None");
		return -1;
	}

	Py_DECREF(self->callback);
	Py_INCREF(value);
	self->callback = value;

	return 0;
}

PyGetSetDef Spam_getseters[] =
{
{ const_cast<char*>("first"), (getter) Spam_getfirst, (setter) Spam_setfirst,
		const_cast<char*>("first name"),
		NULL },
{ const_cast<char*>("last"), (getter) Spam_getlast, (setter) Spam_setlast,
		const_cast<char*>("last name"),
		NULL },
{ const_cast<char*>("callback"), (getter) Spam_getcallback,
		(setter) Spam_setcallback, const_cast<char*>("callback function"),
		NULL },
{ NULL } /* Sentinel */
};

PyObject * Spam_name(Spam* self)
{
	static PyObject *format = NULL;
	PyObject *args, *result;

	if (format == NULL)
	{
		format = PyString_FromString("%s %s");
		if (format == NULL)
			return NULL;
	}

	args = Py_BuildValue("OO", self->first, self->last);
	if (args == NULL)
		return NULL;

	result = PyString_Format(format, args);
	Py_DECREF(args);

	return result;
}

PyObject * Spam_system(Spam *self, PyObject *args)
{
	const char *command;
	int sts;
	if (!PyArg_ParseTuple(args, "s", &command))
		return NULL;
	// execute the command
	sts = system(command);
	// execute callback if any
	if (self->callback && (self->callback != Py_None))
	{
		PyObject *arglist;
		PyObject *result;
		arglist = Py_BuildValue("(s)", command);
		result = PyObject_CallObject(self->callback, arglist);
		Py_DECREF(arglist);
		if (result == NULL)
			return NULL; /* Pass error back */
		Py_DECREF(result);
	}
	return Py_BuildValue("i", sts);
}

PyObject * Spam_system_kw(Spam *self, PyObject *args, PyObject *kwords)
{
	const char *command;
	char *options = const_cast<char*>("");
	static char *kwordlist[] =
	{ const_cast<char*>("cmd"), const_cast<char*>("options"),
	NULL };
	int sts;
	if (!PyArg_ParseTupleAndKeywords(args, kwords, "s|s", kwordlist, &command,
			&options))
		return NULL;
	// execute the command
	string cmdComplete = string(command) + string(" ") + string(options);
	sts = system(cmdComplete.c_str());
	// execute callback if any
	if (self->callback && (self->callback != Py_None))
	{
		PyObject *arglist;
		PyObject *result;
		arglist = Py_BuildValue("(s)", cmdComplete.c_str());
		result = PyObject_CallObject(self->callback, arglist);
		Py_DECREF(arglist);
		if (result == NULL)
			return NULL; /* Pass error back */
		Py_DECREF(result);
	}
	return Py_BuildValue("i", sts);
}

PyMethodDef Spam_methods[] =
{
{ "name", reinterpret_cast<PyCFunction>(Spam_name), METH_NOARGS,
		"Return the name, combining the first and last name" },
{ "system", reinterpret_cast<PyCFunction>(Spam_system), METH_VARARGS,
		"Execute a shell command." },
{ "system_kw", reinterpret_cast<PyCFunction>(Spam_system_kw), METH_VARARGS
		| METH_KEYWORDS, "Execute a shell command." },
{ NULL } /* Sentinel */
};

PyTypeObject SpamType =
{ PyObject_HEAD_INIT(NULL) 0, /*ob_size*/
"spam.Spam", /*tp_name*/
sizeof(Spam), /*tp_basicsize*/
0, /*tp_itemsize*/
(destructor) Spam_dealloc, /*tp_dealloc*/
0, /*tp_print*/
0, /*tp_getattr*/
0, /*tp_setattr*/
0, /*tp_compare*/
0, /*tp_repr*/
0, /*tp_as_number*/
0, /*tp_as_sequence*/
0, /*tp_as_mapping*/
0, /*tp_hash */
0, /*tp_call*/
0, /*tp_str*/
0, /*tp_getattro*/
0, /*tp_setattro*/
0, /*tp_as_buffer*/
Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
"Spam objects", /* tp_doc */
0, /* tp_traverse */
0, /* tp_clear */
0, /* tp_richcompare */
0, /* tp_weaklistoffset */
0, /* tp_iter */
0, /* tp_iternext */
Spam_methods, /* tp_methods */
Spam_members, /* tp_members */
Spam_getseters, /* tp_getset */
0, /* tp_base */
0, /* tp_dict */
0, /* tp_descr_get */
0, /* tp_descr_set */
0, /* tp_dictoffset */
(initproc) Spam_init, /* tp_init */
0, /* tp_alloc */
Spam_new, /* tp_new */
};

PyMethodDef module_methods[] =
{
{ NULL } /* Sentinel */
};

}  // anonymous namespace

#ifndef PyMODINIT_FUNC	/* declarations for DLL import/export */
#define PyMODINIT_FUNC void
#endif
PyMODINIT_FUNC initspam(void)
{
	PyObject* m;

	if (PyType_Ready(&SpamType) < 0)
		return;

	m = Py_InitModule3("spam", module_methods,
			"Example module that creates an extension type.");

	if (m == NULL)
		return;

	Py_INCREF(&SpamType);
	PyModule_AddObject(m, "Spam", reinterpret_cast<PyObject *>(&SpamType));
}
