from spam import Spam

# this is the spam's callback
def callback(cmd):
    """the callback of system"""
    
    print("This command was called: " + cmd)
    
if __name__ == '__main__':
    s = Spam(first="Spam", last="instance", callback=callback)
    print(s.name())
    res = s.system("ls -l")
    res = s.system_kw(options = "-tunlp", cmd = "netstat")
    print(str(res))
    #
#     del s.callback
    s.callback = None
    res = s.system("ls -l")
#     s.callback = "no good"
    s.callback = callback
    res = s.system_kw(options = "-tunlp", cmd = "netstat")
