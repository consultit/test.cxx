# execute as: python setup.py build -b $(pwd) --build-lib $(pwd) -t $(pwd)

from distutils.core import setup, Extension

module1 = Extension('spam',
                    sources = ['spam.cpp'])

setup (name = 'Spam',
       version = '1.0',
       description = 'This is a spam package',
       ext_modules = [module1])