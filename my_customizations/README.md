# my_customizations
Various libraries' build system customizations:
- my_bullet3 (https://github.com/bulletphysics/bullet3.git)
- my_librocket (https://github.com/libRocket/libRocket.git)
- my_opensteer (http://opensteer.sourceforge.net)
- my_raknet (https://github.com/OculusVR/RakNet.git)
- my_recastnavigation (https://github.com/recastnavigation/recastnavigation.git) 

