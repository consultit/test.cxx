/*
 *   This file is part of test.
 *
 *   test is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   test is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with test.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file /test/src/main.cpp
 *
 * \date 28/apr/2014 (10:48:52)
 * \author consultit
 */

#include <iostream>
#include <functional>

void doJob(void (fDoJob)(const std::string&), const std::string& msg)
{
	fDoJob(msg);
}

class Delegate
{
public:
	void doJob(const std::string& msg)
	{
		std::cout << this << "->" << msg << std::endl;
	}
};

void my_job(const std::string& msg)
{
	std::cout << "my_job->" << msg << std::endl;
}

const char* MSG = "Hello World!";

int main(int argc, char** argv)
{
	Delegate d;
	d.doJob(MSG);
	auto db = std::bind(&Delegate::doJob, std::ref(d), std::placeholders::_1);
	std::function<void(const std::string&)> df = db;
	db(MSG);
	df(MSG);
	std::function<void(const std::string&)> mf = my_job;
	mf(MSG);
//	doJob(db, MSG);
//	doJob(df, MSG);
	return 0;
}

