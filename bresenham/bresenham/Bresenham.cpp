/*
 *   This file is part of rasterization.
 *
 *   rasterization is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   rasterization is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with rasterization.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file /rasterization/src/Bresenham.cpp
 *
 * \date 27/apr/2014 (21:04:15)
 * \author consultit
 */

#include <iostream>
#include <graphics.h>
#include <climits>

extern int MAXY;
extern bool LH;
#define Y(coord) \
	LH ? coord : MAXY - coord

#define sign(x) ((x > 0)? 1 : ((x < 0)? -1: 0))

void lineBresenham(int x1, int y1, int x2, int y2, int* minX = NULL, int* maxX =
NULL, int* minY = NULL, int* maxY = NULL, bool drawPixel = true);

using namespace std;

void main_BresenhamLine()
{
	int x1, x2, y1, y2;

	char ans;
	cout << "Draw line? (y/n)" << endl;
	cin >> ans;
	if ((ans != 'y') and (ans != 'Y'))
	{
		return;
	}
	do
	{
		cleardevice();
		line(320, 0, 320, 480);
		line(0, 240, 640, 240);
		cout << "x1   : ";
		cin >> x1;
		cout << "y1   : ";
		cin >> y1;
		cout << "x2   : ";
		cin >> x2;
		cout << "y2   : ";
		cin >> y2;
		lineBresenham(x1, x2, y1, y2);
		cout << endl << "continue ? (y/n)";
		cin >> ans;
	} while ((ans == 'y') or (ans == 'Y'));
}

//https://www.cs.umd.edu/class/fall2003/cmsc427/bresenham.html

void lineBresenham(int x1, int x2, int y1, int y2, int* minX, int* maxX,
		int* minY, int* maxY, bool drawPixel)
{
	int x = x1;
	int y = y1;
	int dx = abs(x2 - x1);
	int dy = abs(y2 - y1);
	int s1 = sign(x2 - x1);
	int s2 = sign(y2 - y1);
	int swap = 0;
	if (dy > dx)
	{
		int temp = dx;
		dx = dy;
		dy = temp;
		swap = 1;
	}
	int D = 2 * dy - dx;
	for (int i = 0; i < dx; i++)
	{
		if (drawPixel)
		{
			putpixel(x, Y(y), 3);
		}
		else
		{
			//set minX and maxX
			minX[y] > x ? minX[y] = x : 0;
			maxX[y] < x ? maxX[y] = x : 0;
			//set minY and maxY
			*minY > y ? *minY = y : 0;
			*maxY < y ? *maxY = y : 0;
		}

		while (D >= 0)
		{
			D = D - 2 * dx;
			if (swap)
				x += s1;
			else
				y += s2;
		}
		D = D + 2 * dy;
		if (swap)
			y += s2;
		else
			x += s1;
	}
}

////////////////////////////////
void circleBresenham(int xc, int yc, int r, int* minX = NULL, int* maxX = NULL,
		int* minY = NULL, int* maxY = NULL, bool drawPixel = true);
void drawCircle(int xc, int yc, int x, int y);
void setBoundaries(int xc, int yc, int x, int y, int* minX, int* maxX,
		int* minY, int* maxY);
void checkBoundary(int x, int y, int* minX, int* maxX, int* minY, int* maxY);

void main_BresenhamCircle()
{
	int xc, yc, r;

	char ans;
	cout << "Draw circle? (y/n)" << endl;
	cin >> ans;
	if ((ans != 'y') and (ans != 'Y'))
	{
		return;
	}
	do
	{
		cleardevice();
		line(320, 0, 320, 480);
		line(0, 240, 640, 240);
		cout << "xc   : ";
		cin >> xc;
		cout << "yc   : ";
		cin >> yc;
		cout << "r   : ";
		cin >> r;
		circleBresenham(xc, yc, r);
		cout << "continue ? (y/n)";
		cin >> ans;
	} while ((ans == 'y') or (ans == 'Y'));
}

void circleBresenham(int xc, int yc, int r, int* minX, int* maxX, int* minY,
		int* maxY, bool drawPixel)
{
	int x = 0, y = r;
	int d = 3 - 2 * r;

	while (x < y)
	{
		if (drawPixel)
		{
			drawCircle(xc, yc, x, y);
		}
		else
		{
			setBoundaries(xc, yc, x, y, minX, maxX, minY, maxY);
		}
		x++;
		if (d < 0)
			d = d + 4 * x + 6;
		else
		{
			y--;
			d = d + 4 * (x - y) + 10;
		}
		if (drawPixel)
		{
			drawCircle(xc, yc, x, y);
		}
		else
		{
			setBoundaries(xc, yc, x, y, minX, maxX, minY, maxY);
		}
	}
}

void checkBoundary(int x, int y, int* minX, int* maxX, int* minY, int* maxY)
{
	//set minX and maxX
	minX[y] > x ? minX[y] = x : 0;
	maxX[y] < x ? maxX[y] = x : 0;
	//set minY and maxY
	*minY > y ? *minY = y : 0;
	*maxY < y ? *maxY = y : 0;
}

void setBoundaries(int xc, int yc, int x, int y, int* minX, int* maxX,
		int* minY, int* maxY)
{
	checkBoundary(xc + x, yc + y, minX, maxX, minY, maxY); //1
	checkBoundary(xc - x, yc + y, minX, maxX, minY, maxY); //2
	checkBoundary(xc + x, yc - y, minX, maxX, minY, maxY); //3
	checkBoundary(xc - x, yc - y, minX, maxX, minY, maxY); //4
	checkBoundary(xc + y, yc + x, minX, maxX, minY, maxY); //5
	checkBoundary(xc - y, yc + x, minX, maxX, minY, maxY); //6
	checkBoundary(xc + y, yc - x, minX, maxX, minY, maxY); //7
	checkBoundary(xc - y, yc - x, minX, maxX, minY, maxY); //8
}

void drawCircle(int xc, int yc, int x, int y)
{
	putpixel(xc + x, Y(yc + y), RED); //1
	putpixel(xc - x, Y(yc + y), RED); //2
	putpixel(xc + x, Y(yc - y), RED); //3
	putpixel(xc - x, Y(yc - y), RED); //4
	putpixel(xc + y, Y(yc + x), RED); //5
	putpixel(xc - y, Y(yc + x), RED); //6
	putpixel(xc + y, Y(yc - x), RED); //7
	putpixel(xc - y, Y(yc - x), RED); //8
}

/////////////////////
void rasterizeTriangle(int x1, int y1, int x2, int y2, int x3, int y3,
		int* minX, int* maxX, int* minY, int* maxY);
void main_RasterizeTriangle()
{
	char ans;
	cout << "Rasterize triangle? (y/n)" << endl;
	cin >> ans;
	if ((ans != 'y') and (ans != 'Y'))
	{
		return;
	}
	//initialize minX and maxX
	int dimY = getmaxy();
	int* minX = new int[dimY];
	int* maxX = new int[dimY];
	for (int i = 0; i < dimY; ++i)
	{
		minX[i] = INT_MAX;
		maxX[i] = INT_MIN;
	}
	int minY = INT_MAX;
	int maxY = INT_MIN;

	int x1, y1, x2, y2, x3, y3;

	do
	{
		cleardevice();
		line(320, 0, 320, 480);
		line(0, 240, 640, 240);
		cout << "x1   : ";
		cin >> x1;
		cout << "y1   : ";
		cin >> y1;
		cout << "x2   : ";
		cin >> x2;
		cout << "y2   : ";
		cin >> y2;
		cout << "x3   : ";
		cin >> x3;
		cout << "y3   : ";
		cin >> y3;
		rasterizeTriangle(x1, y1, x2, y2, x3, y3, minX, maxX, &minY, &maxY);
		cout << "continue ? (y/n)";
		cin >> ans;
	} while ((ans == 'y') or (ans == 'Y'));

	delete[] minX;
	delete[] maxX;
}

void rasterizeTriangle(int x1, int y1, int x2, int y2, int x3, int y3,
		int* minX, int* maxX, int* minY, int* maxY)
{
	//projection if from up to down
	//cull back facing triangles: see "D.H. Eberly: 3D Game Engine Design, 2nd edition"
	int det = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
	if (det < 0)
	{
		return;
	}

	//calculate boundaries by (not) drawing the edges
	lineBresenham(x1, x2, y1, y2, minX, maxX, minY, maxY, false);
	lineBresenham(x2, x3, y2, y3, minX, maxX, minY, maxY, false);
	lineBresenham(x3, x1, y3, y1, minX, maxX, minY, maxY, false);
	//draw row lines
	for (int row = *minY; row < *maxY + 1; ++row)
	{
		for (int col = minX[row]; col < maxX[row] + 1; ++col)
		{
			putpixel(col, Y(row), YELLOW);
		}
		//reset X boundaries
		minX[row] = INT_MAX;
		maxX[row] = INT_MIN;
	}
	//reset Y boundaries
	*minY = INT_MAX;
	*maxY = INT_MIN;
}

/////////////////////
void rasterizeCircle(int xc, int yc, int r, int* minX, int* maxX, int* minY,
		int* maxY);
void main_RasterizeCircle()
{
	char ans;
	cout << "Rasterize circle? (y/n)" << endl;
	cin >> ans;
	if ((ans != 'y') and (ans != 'Y'))
	{
		return;
	}
	//initialize minX and maxX
	int dimY = getmaxy();
	int* minX = new int[dimY];
	int* maxX = new int[dimY];
	for (int i = 0; i < dimY; ++i)
	{
		minX[i] = INT_MAX;
		maxX[i] = INT_MIN;
	}
	int minY = INT_MAX;
	int maxY = INT_MIN;

	int xc, yc, r;

	do
	{
		cleardevice();
		line(320, 0, 320, 480);
		line(0, 240, 640, 240);
		cout << "xc   : ";
		cin >> xc;
		cout << "yc   : ";
		cin >> yc;
		cout << "r   : ";
		cin >> r;
		rasterizeCircle(xc, yc, r, minX, maxX, &minY, &maxY);
		cout << "continue ? (y/n)";
		cin >> ans;
	} while ((ans == 'y') or (ans == 'Y'));

	delete[] minX;
	delete[] maxX;
}

void rasterizeCircle(int xc, int yc, int r, int* minX, int* maxX, int* minY,
		int* maxY)
{

	//calculate boundaries by (not) drawing the circle
	circleBresenham(xc, yc, r, minX, maxX, minY, maxY, false);
	//draw row lines
	for (int row = *minY; row < *maxY + 1; ++row)
	{
		for (int col = minX[row]; col < maxX[row] + 1; ++col)
		{
			putpixel(col, Y(row), RED);
		}
		//reset X boundaries
		minX[row] = INT_MAX;
		maxX[row] = INT_MIN;
	}
	//reset Y boundaries
	*minY = INT_MAX;
	*maxY = INT_MIN;
}

