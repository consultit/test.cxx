/*
 *   This file is part of test.
 *
 *   test is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   test is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with test.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file /test/src/main_bresenham.cpp
 *
 * \date 14/08/2014 (11:55:39)
 * \author consultit
 */

#include <iostream>

#ifdef __cplusplus
extern "C"
{
#endif		/* __cplusplus */

void initgraph(int *graphdriver, int *graphmode, char *pathtodriver);
void closegraph(void);
int  getmaxy(void);
/* graphic drivers */
enum _driver
{
	DETECT = 0, USER, VGA = 9
};
enum graphics_modes
{
	VGALO = 0, VGAMED, VGAHI, VGAMAX, VGA640, VGA800, VGA1024, USERMODE
};
/* 16 colors */
enum _color
{
	BLACK = 0,
	BLUE,
	GREEN,
	CYAN,
	RED,
	MAGENTA,
	BROWN,
	LIGHTGRAY,
	DARKGRAY,
	LIGHTBLUE,
	LIGHTGREEN,
	LIGHTCYAN,
	LIGHTRED,
	LIGHTMAGENTA,
	YELLOW,
	WHITE
};
#ifdef __cplusplus
}
#endif		/* __cplusplus */

void main_BresenhamLine();
void main_BresenhamCircle();
void main_RasterizeTriangle();
void main_RasterizeCircle();

int MAXY;
bool LH;

using namespace std;

int main_bresenham(int argc, char** argv)
{
	int gDriver = DETECT, gMode = GREEN;
	initgraph(&gDriver, &gMode, 0);

	//left handed
	MAXY = getmaxy();
	LH = false;
	char ans;
	cout << endl << "Left handed frame? (y/n) [default n]";
	cin >> ans;
	if ((ans == 'y') or (ans == 'Y'))
	{
		MAXY = 0;
		LH = true;
	}

	main_BresenhamLine();
	main_BresenhamCircle();
	main_RasterizeTriangle();
	main_RasterizeCircle();

	closegraph();
	return 0;
}

