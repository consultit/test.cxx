/*
 *   This file is part of test.
 *
 *   test is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   test is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with test.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file /test/src/main.cpp
 *
 * \date 28/apr/2014 (10:48:52)
 * \author consultit
 */

int main_bresenham(int argc, char** argv);
int main_physics(int argc, char** argv);
int main_poly2D_intersection(int argc, char** argv);

int main(int argc, char** argv)
{
	int result;

	result = main_bresenham(argc, argv);
//	result = main_physics(argc, argv);
//	result = main_poly2D_intersection(argc, argv);

	return result;
}

