//============================================================================
// Name        : test1.cpp
// Author      : pippo
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
#include "Vec3.h"
using namespace std;
using namespace OpenSteer;

///Check two convex polygons 2D intersection (by using separating axis algorithm).
///Any point or vertex are first projected over the y=0 plane, and resulting
///projected vertices are supposed to be defined ccw looking down y axis.
///No check is done.
///See "D.H. Eberly: 3D Game Engine Design, 2nd edition"
bool seekSeparatingAxis(Vec3* poly1Vert, int vertN1, Vec3* poly2Vert,
		int vertN2);
bool polyIntersect(Vec3* poly1Vert, int vertN1, Vec3* poly2Vert, int vertN2)
{
	//let's work in y=0 plane
	Vec3* poly1V = new Vec3[vertN1];
	Vec3* poly2V = new Vec3[vertN2];
	for (int i = 0; i < vertN1; ++i)
	{
		poly1V[i] = poly1Vert[i].setYtoZero();
	}
	for (int i = 0; i < vertN2; ++i)
	{
		poly2V[i] = poly2Vert[i].setYtoZero();
	}
	//
	bool foundIntersection = false;
	if (not seekSeparatingAxis(poly1V, vertN1, poly2V, vertN2))
	{
		foundIntersection = not seekSeparatingAxis(poly2V, vertN2, poly1V,
				vertN1);
	}
	//free resources
	delete[] poly1V;
	delete[] poly2V;
	//
	return foundIntersection;
}

bool seekSeparatingAxis(Vec3* poly1V, int vertN1, Vec3* poly2V, int vertN2)
{
	bool separatingAxisFound = false;
	//check poly1 normals vs poly2 vertices
	//for each poly1 external normal do
	for (int n = 0; n < vertN1; ++n)
	{
		int m = (n + 1) % vertN1;
		//get external normal as unit vector
		Vec3 edge = (poly1V[m] - poly1V[n]);
		Vec3 crossV;
		crossV.cross(edge, Vec3(0, 1, 0));
		Vec3 normal = crossV.normalize();
		//check every poly2 vertex component wrt normal
		for (int t = 0; t < vertN2; ++t)
		{
			//get the poly2 vertex component wrt normal and poly1 vertex:
			//component = (normal) dot (poly2Vert - poly1Vert)
			float component = (poly2V[t] - poly1V[n]).dot(normal);
			if (component < 0.0)
			{
				break;
			}
			if (t == vertN2 - 1)
			{
				//found a separating axis
				separatingAxisFound = true;
			}
		}
		if (separatingAxisFound)
		{
			break;
		}
	}
	return separatingAxisFound;
}

///Check rectangle vs circle 2D intersection.
///Any point or vertex are first projected over the y=0 plane, and resulting
///projected vertices are supposed to form a rectangle defined ccw looking down y axis.
///No check is done.
///See http://www.geometrictools.com/Documentation/IntersectionRectangleEllipse.pdf.
bool edgeCircleIntersect(Vec3 edgeV1, Vec3 edgeV2, Vec3 center, float radius);
bool rectangleCircleIntersect(Vec3* rectVertices, Vec3 circleCenter,
		float circleRadius)
{
	bool foundIntersection = false;
	//let's work in y=0 plane
	Vec3 rectV[4];
	for (int i = 0; i < 4; ++i)
	{
		rectV[i] = rectVertices[i].setYtoZero();
	}
	Vec3 circleC = circleCenter.setYtoZero();
	/*
		3-----------2      ----
		|     u2    |     /    \
		|     |     |    /      \
		|    rC--u1 |   |   cC   |
		|           |    \      /
		0-----------1     \    /
	                       ----
	*/
	//get rectangle half dimensions
	float halfDim1 = (rectV[1] - rectV[0]).length() / 2.0;
	float halfDim2 = (rectV[3] - rectV[0]).length() / 2.0;
	assert((halfDim1 > 0) and (halfDim2 > 0));
	//get rectangle frame unit vectors (origin=rectangle center)
	Vec3 rectU1 = (rectV[1] - rectV[0]).normalize();
	Vec3 rectU2 = (rectV[2] - rectV[1]).normalize();
	//get rectangle center
	Vec3 rectC = rectV[0] + rectU1 * halfDim1 + rectU2 * halfDim2;
	//transform circle center wrt the rectangle frame
	Vec3 circleCR = Vec3(rectU1.dot(circleC - rectC), 0,
			rectU2.dot(circleC - rectC));
	//check if circle center is inside rectangle
	if ((abs(circleCR.x) <= halfDim1) and (abs(circleCR.z) <= halfDim2))
	{
		//intersection
		foundIntersection = true;
	}
	else
	{
		//transform rectangle's vertices wrt the rectangle frame
		Vec3 rectVR[4];
		for (int i = 0; i < 4; ++i)
		{
			rectVR[i] = Vec3(rectU1.dot(rectV[i] - rectC), 0,
					rectU2.dot(rectV[i] - rectC));
		}
		//check circle vs rectangle's edges intersection
		//according to which quadrant there is the circle center
		int edge1Idx[2], edge2Idx[2];
		//1st quadrant: check 2-1 and 3-2 edges
		if ((circleCR.x > 0) and (circleCR.z > 0))
		{
			edge1Idx[0] = 1;
			edge1Idx[1] = 2;
			edge2Idx[0] = 2;
			edge2Idx[1] = 3;
		}
		//2nd quadrant: check 3-2 and 0-3 edges
		else if ((circleCR.x < 0) and (circleCR.z > 0))
		{
			edge1Idx[0] = 2;
			edge1Idx[1] = 3;
			edge2Idx[0] = 3;
			edge2Idx[1] = 0;
		}
		//3rd quadrant: check 0-3 and 1-0 edges
		else if ((circleCR.x < 0) and (circleCR.z < 0))
		{
			edge1Idx[0] = 3;
			edge1Idx[1] = 0;
			edge2Idx[0] = 0;
			edge2Idx[1] = 1;
		}
		//4th quadrant: check 1-0 and 2-1 edges
		else if ((circleCR.x > 0) and (circleCR.z < 0))
		{
			edge1Idx[0] = 0;
			edge1Idx[1] = 1;
			edge2Idx[0] = 1;
			edge2Idx[1] = 2;
		}
		foundIntersection = edgeCircleIntersect(rectVR[edge1Idx[0]],
				rectVR[edge1Idx[1]], circleCR, circleRadius);
		if (not foundIntersection)
		{
			foundIntersection = edgeCircleIntersect(rectVR[edge2Idx[0]],
					rectVR[edge2Idx[1]], circleCR, circleRadius);
		}
	}
	//
	return foundIntersection;
}

bool edgeCircleIntersect(Vec3 edgeV0, Vec3 edgeV1, Vec3 center, float radius)
{
	//by default no intersection
	bool foundIntersection = false;
	//get quadratic expression: q2*t^2 + q1*t + q0
	float radius2 = radius * radius;
	float q2 = (edgeV1 - edgeV0).lengthSquared() / radius2;
	float q1 = 2 * (edgeV1 - edgeV0).dot(edgeV0 - center) / radius2;
	float q0 = (edgeV0 - center).lengthSquared() / radius2 - 1.0;
	//compute discriminant
	float discriminant = q1 * q1 - 4 * q2 * q0;
	//check if there are solutions: discriminant >= 0
	if (discriminant >= 0.0)
	{
		//there are 1 or 2 solutions: check if:
		//- at least one is in [0, 1] or
		//- they are opposite in sign (meaning that one
		//	is >1 and the other is <0, i.e. edge is contained
		//	entirely in circle)
		float t1 = (-q1 + sqrt(discriminant)) / (2 * q2);
		float t2 = (-q1 - sqrt(discriminant)) / (2 * q2);
		if ((0 <= t1 and t1 <= 1.0) or (0 <= t2 and t2 <= 1.0)
				or (t1 * t2 < 0.0))
		{
			foundIntersection = true;
		}
	}
	//
	return foundIntersection;
}

Vec3 mapVert[4] =
{ Vec3(44.7217, 101, 98.1624), Vec3(65.1996, 200, 79.0665), Vec3(35.3622, 301,
		47.0698), Vec3(14.8843, 100, 66.1657) };
Vec3 triVert[3] =
{ Vec3(15.582, 100, 97.0888), Vec3(56.2852, 100, 78.0362), Vec3(52.3125, 100, 19.4336) };

Vec3 circleCenter(34.8242, 115.313, 96.9141);
float circleRadius = 15.0941;

int main_poly2D_intersection(int argc, char** argv)
{
	cout
			<< polyIntersect(mapVert, sizeof(mapVert) / sizeof(Vec3), triVert,
					sizeof(triVert) / sizeof(Vec3)) << endl;
	cout << rectangleCircleIntersect(mapVert, circleCenter, circleRadius)
			<< endl;
	return 0;
}
