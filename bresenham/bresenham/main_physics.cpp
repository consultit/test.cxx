//============================================================================
// Name        : test1.cpp
// Author      : consultit
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdlib>
using namespace std;

///XXX all is in seconds
//http://gafferongames.com/game-physics/fix-your-timestep/

double time_in_seconds = 0.0; //real time
double t = 0.0; //simulation time
const double dt = 1 / 60.0; //max integration step duration
const double integrateDt = dt * 1.0e-1; //effective integration step duration
const double renderDt = 4.0 * dt * 1.0e+0; //effective render step duration

void integrate(double state, double t, double deltaTime)
{
	//state = duration of integration (step)
	time_in_seconds += state;
}
void render(double state)
{
	//state = duration of rendering
	time_in_seconds += state;
}
double hires_time_in_seconds()
{
	return time_in_seconds;
}

int main_physics(int argc, char** argv)
{
	bool quit = false;

	double currentTime = hires_time_in_seconds();
	double accumulator = 0.0;

	while (!quit)
	{
		double newTime = hires_time_in_seconds();
		double frameTime = newTime - currentTime;
		currentTime = newTime;

		accumulator += frameTime;

		while (accumulator >= dt)
		{
			integrate(integrateDt, t, dt);
			accumulator -= dt;
			t += dt;
		}
		float fact = (float) rand() / (float) RAND_MAX;
		render(renderDt * fact + renderDt * 1.0e-10);

		///lag = (current time) - (physics simulation time)
		double lag = hires_time_in_seconds() - t;
		cout << "(" << fact << ") " << hires_time_in_seconds() << " - " << t
				<< " = " << lag << endl;
		///

	}

	return 0;
}
