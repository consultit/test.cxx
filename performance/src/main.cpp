/*
 *   This file is part of test.
 *
 *   test is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   test is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with test.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file /test/src/main.cpp
 *
 * \date 28/apr/2014 (10:48:52)
 * \author consultit
 */

#include <iostream>
#include <ctime>

using namespace std;
#define PRINT_RESULT(msg,t) cout << msg " (ms): " << \
	(float) (clock() - t) * 1000 / CLOCKS_PER_SEC << endl;
///Aligned vs un-aligned
class ALIGNED
{
	int i __attribute__ ((aligned (16)));
	char c __attribute__ ((aligned (16)));
public:
	int getI()
	{
		return i;
	}
	void setI(int _i)
	{
		i = _i;
	}
};
class UNALIGNED
{
	int i;
	char c;
public:
	int getI()
	{
		return i;
	}
	void setI(int _i)
	{
		i = _i;
	}
};
const unsigned int NUM = 1000000000;
ALIGNED a[4];
UNALIGNED u[4];
void alignedVsUnaligned();
///

///data proximity (L1/L2/L3 cache)
const int g_n = 500;
float TestData[g_n][g_n][g_n];
void columnVsRowOrdered();
///

int main(int argc, char** argv)
{
	cout << "alignedVsUnaligned" << endl;
	alignedVsUnaligned();
	cout << endl;

	cout << "columnVsRowOrdered" << endl;
	columnVsRowOrdered();
	cout << endl;
	return 0;
}

void alignedVsUnaligned()
{
	clock_t t;
	//measure
	t = clock();
	//perform task
	for (unsigned int j = 0; j < NUM; ++j)
	{
		int s = 0;
		for (int k = 0; k < 4; ++k)
		{
			a[k].setI(j % (k + 1));
			s += a[k].getI();
			a[k].setI(s);
		}
	}
	//print results
	PRINT_RESULT("aligned", t)
	//measure
	t = clock();
	//perform task
	for (unsigned int j = 0; j < NUM; ++j)
	{
		int s = 0;
		for (int k = 0; k < 4; ++k)
		{
			u[k].setI(j % (k + 1));
			s += u[k].getI();
			u[k].setI(s);
		}
	}
	//print results
	PRINT_RESULT("unaligned", t)
}

void columnVsRowOrdered()
{
	clock_t t;
	//measure
	t = clock();
	//perform task
	//column by column
	for (int k = 0; k < g_n; k++)
		for (int j = 0; j < g_n; j++)
			for (int i = 0; i < g_n; i++)
				TestData[i][j][k] = 0.0f;
	//print results
	PRINT_RESULT("column^2", t)

	//measure
	t = clock();
	//perform task
	//row by row
	for (int i = 0; i < g_n; i++)
		for (int j = 0; j < g_n; j++)
			for (int k = 0; k < g_n; k++)
				TestData[i][j][k] = 0.0f;
	//print results
	PRINT_RESULT("row^2", t)
}
